FROM alpine:3.6
LABEL maintainer "guillaume.duveau@gmail.com"

# Ensure www-data user exists
RUN addgroup -g 82 -S www-data && \
    adduser -u 82 -D -S -G www-data www-data

# Upgrade
RUN apk upgrade --no-cache

# Install general tools
RUN apk add --no-cache \
            curl \
            git

# Install PHP with extensions
RUN apk add --no-cache \
            php7 \
            php7-bcmath \
            php7-dom \
            php7-ctype \
            php7-curl \
            php7-fileinfo \
            php7-gd \
            php7-iconv \
            php7-intl \
            php7-json \
            php7-mbstring \
            php7-mcrypt \
            php7-mysqlnd \
            php7-opcache \
            php7-openssl \
            php7-pdo \
            php7-pdo_mysql \
            php7-pdo_pgsql \
            php7-pdo_sqlite \
            php7-phar \
            php7-posix \
            php7-session \
            php7-simplexml \
            php7-soap \
            php7-tokenizer \
            php7-xml \
            php7-xmlreader \
            php7-xmlwriter \
            php7-zip \
            php7-zlib

# Configure PHP
COPY etc/php7/conf.d/*.ini /etc/php7/conf.d/

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Drush
USER www-data
RUN composer global require drush/drush && \
    composer clear-cache

# Install Drupal Console
USER root
RUN curl https://drupalconsole.com/installer -L -o drupal.phar && \
    mv drupal.phar /usr/local/bin/drupal && \
    chmod +x /usr/local/bin/drupal

# Install Node
RUN apk add --no-cache \
            nodejs \
            nodejs-npm

# Install Bower, Gulp and Grunt
RUN npm install -g \
                bower \
                gulp-cli \
                grunt-cli

# Allow Bower to run as root
RUN echo '{ "allow_root": true }' > /root/.bowerrc

# Define default command.
CMD ["bash"]
